# {{ name }}
{% if __project_description__ %}
{{ __project_description__ }}
{% endif %}
## Introduction

This is a BlackDynamite project (Using Renku).
The documentation for the philosophy of `BlackDynamite` can be found [here](https://gitlab.com/ganciaux/blackdynamite). 

## Starting the project

The project description can be found in the file [bd.yaml](./bd.yaml). You certainly want to adapt it to your needs.

To help you get started a simple parametric space has been described (open [bd.yaml](./bd.yaml) to see it).

The study is attached with two scripts

- [launch.sh](./launch.sh) : an entry point
- [doIt.py](./doIt.py) : a real script that could be whatever you want

## Creating the database and populate with jobs and runs

To create the database and populate for jobs (parametric space), you can do the following in a terminal:

```sh
createDB --truerun
createJobs --truerun
```

To populate for runs you can do

```sh
createRuns.py --truerun --machine_name lsmspc35 --nproc 1 --run_name 1
```

Then you can observe the generated runs

```sh
getRunInfos
```

You can claim for information over the parametric space:


```sh
getRunInfo.py --infos param1,param2
```


## Launching the execution of runs

To execute all runs in state `CREATED` you can do:

```sh
launchRuns --truerun
```

Then you can check for the completion of your jobs 

```sh
getRunInfo.py
```

## Postprocessing the data

This is usually done in python scripts such as [./notebooks/post_treatment.ipynb](./notebooks/post_treatment.ipynb)
Contributions
-------------

* Author #1  did contribute to
* Author #2  did contribute to

Data collection: period and details
-----------------------------------

* From XXX to XXX, ...

Funding sources
---------------

* Add the funding sources

Data structure and information
------------------------------

Put a description of the data structure

*Hint:*

JTCAM curation policy
---------------------

**First things first: JTCAM data editors are here to help!**

**Second: the JTCAM data curation policy** can be found here: https://zenodo.org/communities/jtcam/curation-policy

JTCAM editors wish to follow the FAIR principles regarding the open data and algorithms: https://en.wikipedia.org/wiki/FAIR_data

The intention is to publish data relevant to the JTCAM paper. This includes:

- Raw data used to produce Figures
- Experimental data
- Simulation Input and Output
- Algorithms (software, scripts)


In essence, this *Open Data* approach will allow **reproducibility** and an **easy analysis by another research group**.

Data structure and information
------------------------------

*Authors are required to provide one **README.md** file per directory to help
 navigation through the data*

**Here the authors are expected to provide a full description of the directories
  hierarchy with a consise description. For instance it could be:**


+ Folder/files structure:
  + `Case 1/`       - folder containing results for case 1
	+ `README.md`              - Description
    + `Case1_EXP_*.csv`        - experimental data files
	+ `Case1_SIM_INPUT*.inp`   - simulation input
    + `Case1_SIM_OUTPUT*.csv`  - simulation output 
  + `Case2/`        - folder containing results for case 2
    + `README.md`              - Description
	+ `Case2_EXP_*.csv`        - experimental data files 
	+ `Case2_SIM_*.csv`        - simulation output 
  + `PlotScripts/`  - folder containing plotting scripts if any
    + `README.md`              - Description of the plotting scripts
	+ `plot_fig1-10.py`        - python script plotting figure 1-10
    + `plot_fig10-20.py`       - python script plotting figure 1-20



Contributions
-------------

* Author #1  did contribute to
* Author #2  did contribute to

Data collection: period and details
-----------------------------------

* From XXX to XXX, ...

Funding sources
---------------

* Add the funding sources

Data structure and information
------------------------------

Put a description of the data structure

*Hint:*

*Authors are required to provide one **README.md** file per directory to help
 navigation through the data*

**Here the authors are expected to provide a full description of the directories
  hierarchy with a consise description. For instance it could be:**


+ Folder/files structure:
  + `Case 1/`       - folder containing results for case 1
	+ `README.md`              - Description
    + `Case1_EXP_*.csv`        - experimental data files
	+ `Case1_SIM_INPUT*.inp`   - simulation input
    + `Case1_SIM_OUTPUT*.csv`  - simulation output 
  + `Case2/`        - folder containing results for case 2
    + `README.md`              - Description
	+ `Case2_EXP_*.csv`        - experimental data files 
	+ `Case2_SIM_*.csv`        - simulation output 
  + `PlotScripts/`  - folder containing plotting scripts if any
    + `README.md`              - Description of the plotting scripts
	+ `plot_fig1-10.py`        - python script plotting figure 1-10
    + `plot_fig10-20.py`       - python script plotting figure 1-20



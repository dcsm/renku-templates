# DCSM Renku project templates

A repository of Computational Solid Mechanics templates for new [Renku](https://renkulab.io/) projects.

## Contributing

If you want to contribute a new template, have a look at the [Renku template documentation](https://renku.readthedocs.io/en/latest/reference/templates.html) to see how a template should be structured and where additional information might be needed (_e.g._ in `manifest.yaml`).

